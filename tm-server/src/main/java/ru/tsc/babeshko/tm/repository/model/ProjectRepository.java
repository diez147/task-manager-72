package ru.tsc.babeshko.tm.repository.model;

import org.springframework.stereotype.Repository;
import ru.tsc.babeshko.tm.model.Project;

@Repository
public interface ProjectRepository extends AbstractUserOwnedRepository<Project> {

}