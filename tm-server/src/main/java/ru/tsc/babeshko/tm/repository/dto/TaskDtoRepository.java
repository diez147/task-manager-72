package ru.tsc.babeshko.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.babeshko.tm.dto.model.TaskDto;
import java.util.List;

@Repository
public interface TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDto> {

    @NotNull List<TaskDto> findAllByProjectIdAndUserId(@NotNull String projectId, @NotNull String userId);

    @Transactional
    void deleteAllByProjectIdAndUserId(@NotNull String projectId, @NotNull String userId);

}