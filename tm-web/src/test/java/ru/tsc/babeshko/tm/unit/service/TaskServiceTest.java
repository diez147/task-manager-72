package ru.tsc.babeshko.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.tsc.babeshko.tm.configuration.ApplicationConfiguration;
import ru.tsc.babeshko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.babeshko.tm.exception.field.EmptyIdException;
import ru.tsc.babeshko.tm.exception.field.EmptyNameException;
import ru.tsc.babeshko.tm.exception.field.EmptyUserIdException;
import ru.tsc.babeshko.tm.marker.UnitCategory;
import ru.tsc.babeshko.tm.model.Task;
import ru.tsc.babeshko.tm.service.TaskService;
import ru.tsc.babeshko.tm.service.UserService;
import ru.tsc.babeshko.tm.util.UserUtil;

import java.util.List;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class TaskServiceTest {

    @NotNull
    @Autowired
    private TaskService taskService;

    @NotNull
    @Autowired
    private UserService userService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private final Task task = new Task("test");

    @NotNull
    private static String USER_ID;

    @Before
    public void init() {
        userService.createUser("test", "test", null);
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
        taskService.save(task, USER_ID);
    }

    @After
    public void destroy() {
        taskService.clearByUserId(USER_ID);
    }

    @Test
    public void add() {
        Assert.assertThrows(EmptyUserIdException.class, () -> taskService.add("test", null));
        Assert.assertThrows(EmptyNameException.class, () -> taskService.add(null, USER_ID));
        taskService.add("test", USER_ID);
        Assert.assertEquals(2, taskService.countByUserId(USER_ID));
    }

    @Test
    public void save() {
        Assert.assertThrows(EmptyUserIdException.class, () -> taskService.save(new Task(), null));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.save(null, USER_ID));
        taskService.save(new Task(), USER_ID);
        Assert.assertEquals(2, taskService.countByUserId(USER_ID));
    }

    @Test
    public void remove() {
        Assert.assertThrows(EmptyUserIdException.class, () -> taskService.remove(task, null));
        taskService.remove(task, USER_ID);
        Assert.assertEquals(0, taskService.countByUserId(USER_ID));
    }

    @Test
    public void removeByIdAndUserId() {
        @NotNull final String taskId = task.getId();
        Assert.assertThrows(EmptyUserIdException.class, () -> taskService.removeByIdAndUserId(taskId, null));
        Assert.assertThrows(EmptyIdException.class, () -> taskService.removeByIdAndUserId(null, USER_ID));
        taskService.removeByIdAndUserId(taskId, USER_ID);
        Assert.assertEquals(0, taskService.countByUserId(USER_ID));
    }

    @Test
    public void findAllByUserId() {
        Assert.assertThrows(EmptyUserIdException.class, () -> taskService.findAllByUserId(null));
        List<Task> tasks = taskService.findAllByUserId(USER_ID);
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void findByIdAndUserId() {
        @NotNull final String taskId = task.getId();
        Assert.assertThrows(EmptyUserIdException.class, () -> taskService.findByIdAndUserId(taskId, null));
        Assert.assertThrows(EmptyIdException.class, () -> taskService.findByIdAndUserId(null, USER_ID));
        Task taskFromDB = taskService.findByIdAndUserId(taskId, USER_ID);
        Assert.assertNotNull(task);
        Assert.assertEquals(taskId, taskFromDB.getId());
    }

    @Test
    public void existsByIdAndUserId() {
        @NotNull final String taskId = task.getId();
        Assert.assertThrows(EmptyUserIdException.class, () -> taskService.existsByIdAndUserId(taskId, null));
        Assert.assertThrows(EmptyIdException.class, () -> taskService.existsByIdAndUserId(null, USER_ID));
        boolean exists = taskService.existsByIdAndUserId(taskId, USER_ID);
        Assert.assertTrue(exists);
    }

    @Test
    public void clearByUserId() {
        Assert.assertThrows(EmptyUserIdException.class, () -> taskService.clearByUserId(null));
        taskService.clearByUserId(USER_ID);
        Assert.assertEquals(0, taskService.countByUserId(USER_ID));
    }

    @Test
    public void countByUserId() {
        Assert.assertThrows(EmptyUserIdException.class, () -> taskService.countByUserId(null));
        long count = taskService.countByUserId(USER_ID);
        Assert.assertEquals(1L, count);
    }

}
