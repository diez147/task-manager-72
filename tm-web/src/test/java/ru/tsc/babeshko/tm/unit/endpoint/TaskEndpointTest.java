package ru.tsc.babeshko.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.tsc.babeshko.tm.configuration.ApplicationConfiguration;
import ru.tsc.babeshko.tm.marker.UnitCategory;
import ru.tsc.babeshko.tm.model.Task;
import ru.tsc.babeshko.tm.service.TaskService;
import ru.tsc.babeshko.tm.service.UserService;
import ru.tsc.babeshko.tm.util.UserUtil;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class TaskEndpointTest {

    @NotNull
    private static final String TASK_URL = "http://localhost:8080/api/task/";

    @NotNull
    @Autowired
    private TaskService taskService;

    @NotNull
    @Autowired
    private UserService userService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    private final Task task = new Task("test");

    @Nullable
    private static String USER_ID;

    @Before
    public void init() {
        userService.createUser("test", "test", null);
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
        save(task);
    }

    @After
    public void destroy() {
        taskService.clearByUserId(USER_ID);
    }

    @Test
    public void saveTest() {
        save(new Task());
        Assert.assertEquals(2, findAll().size());
    }

    @SneakyThrows
    private void save(@NotNull final Task task) {
        @NotNull String url = TASK_URL + "save";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(task);
        mockMvc.perform(
                MockMvcRequestBuilders.post(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(1, findAll().size());
        save(new Task());
        Assert.assertEquals(2, findAll().size());
    }

    @NotNull
    @SneakyThrows
    private List<Task> findAll() {
        @NotNull final String url = TASK_URL + "findAll";
        @NotNull final String json = mockMvc.perform(
                MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, Task[].class));
    }

    @Test
    public void findByIdTest() {
        @Nullable final Task taskFromDB = findById(task.getId());
        Assert.assertNotNull(taskFromDB);
        Assert.assertEquals(task.getId(), taskFromDB.getId());
    }

    @Nullable
    @SneakyThrows
    private Task findById(@NotNull final String id) {
        @NotNull String url = TASK_URL + "findById/" + id;
        @NotNull final String json =
                mockMvc.perform(
                        MockMvcRequestBuilders.get(url).contentType(MediaType.APPLICATION_JSON)
                ).andDo(print()).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        if (json.isEmpty()) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Task.class);
    }

    @Test
    public void existsByIdTest() {
        Assert.assertEquals(true, existsById(task.getId()));
    }

    @SneakyThrows
    public Boolean existsById(@NotNull final String id) {
        @NotNull String url = TASK_URL + "existsById/" + id;
        @NotNull final String json =
                mockMvc.perform(
                        MockMvcRequestBuilders.get(url).contentType(MediaType.APPLICATION_JSON)
                ).andDo(print()).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        if (json.isEmpty()) return false;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Boolean.class);
    }

    @Test
    public void deleteTest() {
        delete(task);
        Assert.assertEquals(0, findAll().size());
    }

    @SneakyThrows
    public void delete(@NotNull final Task task) {
        @NotNull final String url = TASK_URL + "delete";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(task);
        mockMvc.perform(
                MockMvcRequestBuilders.post(url).content(json).contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void deleteByIdTest() {
        Assert.assertEquals(1, findAll().size());
        deleteById(task.getId());
        Assert.assertNull(findById(task.getId()));
        Assert.assertEquals(0, findAll().size());
    }

    @SneakyThrows
    public void deleteById(@NotNull final String id) {
        @NotNull final String url = TASK_URL + "deleteById/" + id;
        mockMvc.perform(
                MockMvcRequestBuilders.delete(url).contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void clearTest() {
        Assert.assertEquals(1, findAll().size());
        clear();
        Assert.assertEquals(0, findAll().size());
    }

    @SneakyThrows
    private void clear() {
        mockMvc.perform(
                MockMvcRequestBuilders.delete(TASK_URL + "clear")
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(1, count());
    }

    @SneakyThrows
    public long count() {
        @NotNull final String countUrl = TASK_URL + "count";
        @NotNull final String json = mockMvc.perform(
                MockMvcRequestBuilders.get(countUrl).contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, Long.class);
    }

}