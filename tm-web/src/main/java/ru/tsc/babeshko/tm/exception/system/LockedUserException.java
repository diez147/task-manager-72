package ru.tsc.babeshko.tm.exception.system;

import ru.tsc.babeshko.tm.exception.AbstractException;

public final class LockedUserException extends AbstractException {

    public LockedUserException() {
        super("Error! User is locked...");
    }

}