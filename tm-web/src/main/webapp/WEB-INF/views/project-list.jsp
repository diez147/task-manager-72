<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>

<h1>Список проектов</h1>

<table width="100%" cellpadding="10" border="1" style="border-collapse: collapse;">
    <tr>
        <th width="200" nowrap="nowrap">ID</th>
        <th width="200" nowrap="nowrap" align="left">Название</th>
        <th width="100%" align="left">Описание</th>
        <th width="150" align="center" nowrap="nowrap">Статус</th>
        <th width="100" align="center" nowrap="nowrap">Дата начала</th>
        <th width="100" align="center" nowrap="nowrap">Дата окончания</th>
        <th width="100" align="center" nowrap="nowrap">Изменить</th>
        <th width="100" align="center" nowrap="nowrap">Удалить</th>
    </tr>
    <c:forEach var="project" items="${projects}">
        <tr>
            <td>
                <c:out value="${project.id}" />
            </td>
            <td>
                <c:out value="${project.name}" />
            </td>
            <td>
                <c:out value="${project.description}" />
            </td>
            <td align="center" nowrap="nowrap">
                <c:out value="${project.status.displayName}" />
            </td>
            <td align="center">
                <fmt:formatDate pattern="dd.MM.yyyy" value="${project.dateBegin}" />
            </td>
            <td align="center">
                <fmt:formatDate pattern="dd.MM.yyyy" value="${project.dateEnd}" />
            </td>
            <td align="center">
                <a href="/project/edit/${project.id}">Изменить</a>
            </td>
            <td align="center">
                <a href="/project/delete/${project.id}">Удалить</a>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/project/create" style="margin-top: 20px">
    <button>Создать</button>
</form>

<jsp:include page="../include/_footer.jsp"/>